package com.jrelax.third.fs.ftp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * FTP服务
 * Created by zengchao on 2016-12-26.
 */
public class FTPService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    private static FTPService instance;
    public static FTPService getInstance(){
        if (instance == null) {
            instance = new FTPService();
        }
        return instance;
    }

    private FTPService() {

    }

    public void start(){
        logger.info("FS:FTP服务启动");
    }

    public void stop(){
        logger.info("FS:FTP服务已停止");
    }
}
